<?php
return [
    'fromSenderEmail' => 'no-reply@lnalebanon.org',
    'cmsUrl' => env('cmsUrl', 'http://localhost:8080'),
    'frontUrl' => env('frontUrl', 'http://localhost:8081'),
    'appName' => 'LNA',

];
