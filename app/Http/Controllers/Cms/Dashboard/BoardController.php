<?php

namespace App\Http\Controllers\Cms\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\Dashboard\BoardRequest;
use App\Services\Cms\Dashboard\BoardService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;


class BoardController extends Controller
{
    use ApiResponse;

    protected $boardService;

    function __construct(BoardService $boardService)
    {
        $this->boardService = $boardService;
    }

    public function getUserBoard(): JsonResponse
    {
        $boards = $this->boardService->getAll();
        return $this->showAll($boards, 200);
    }

    public function create(BoardRequest $request): JsonResponse
    {
        $data = $request->validated();
        $board = $this->boardService->create($data);
        return $this->showOne($board, 201);
    }

    public function delete($id): JsonResponse
    {

        if ($this->boardService->delete($id)) {
            return $this->successMessage('board deleted successfully', 200);
        }
        return $this->errorResponse('board not found ', 404);
    }
}
