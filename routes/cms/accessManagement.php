<?php

use App\Http\Controllers\Cms\AccessManagement\AuthController;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
});

Route::group(['middleware' => ['auth:cms'], 'prefix' => 'auth'], function ($router) {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('change-my-password', [AuthController::class, 'changeMyPassword']);
    Route::post('update-profile', [AuthController::class, 'updateProfile']);
});
