<?php

namespace App\Http\Requests\Cms\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskSortOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'task_id' => ['required'],
            'new_board_id' => ['required'],
            'new_sort_order' => ['required'],
            'old_board_id' => ['required'],
            'old_index'=> ['required'],
        ];
    }
}
