<?php
use App\Http\Controllers\Cms\Dashboard\BoardController;
use App\Http\Controllers\Cms\Dashboard\TaskController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:cms'], 'prefix' => 'dashboard'], function ($router) {
    Route::get('/get-my-boards', [BoardController::class, 'getUserBoard']);
    Route::post('create', [BoardController::class, 'create']);
    Route::delete('delete/{id}', [BoardController::class, 'delete']);
});


Route::group(['middleware' => ['auth:cms'], 'prefix' => 'tasks'], function ($router) {
    Route::post('create', [TaskController::class, 'create']);
    Route::delete('delete/{id}', [TaskController::class, 'delete']);
    Route::post('update-task-sort-order', [TaskController::class, 'updateTaskSortOrder']);
});
