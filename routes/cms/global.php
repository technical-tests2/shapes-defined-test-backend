<?php


use App\Http\Controllers\Cms\ImageController;
use Illuminate\Support\Facades\Route;


//global routes no need for permissions here

Route::group(['middleware' => ['auth:cms'], 'prefix' => 'files'], function ($router) {
    Route::post('upload-photo', [ImageController::class, 'uploadPhoto']);
    Route::post('delete-photo', [ImageController::class, 'deletePhoto']);
});








