<?php

namespace App\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

trait ApiResponse
{
    protected function successMessage($message, $code): \Illuminate\Http\JsonResponse
    {
        return response()->json(['message' => $message, 'code' => $code], $code);
    }

    protected function errorResponse($message, $code): \Illuminate\Http\JsonResponse
    {
        return response()->json(['message' => $message, 'code' => $code], $code);
    }

    protected function paginatedResponse(LengthAwarePaginator $lengthAwarePaginator, $collection, $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'data' => $collection,
            'pagination' => [
                'total' => $lengthAwarePaginator->total(),
                'count' => $lengthAwarePaginator->count(),
                'per_page' => $lengthAwarePaginator->perPage(),
                'current_page' => $lengthAwarePaginator->currentPage(),
                'total_pages' => $lengthAwarePaginator->lastPage(),
            ],
        ]);
    }


    protected function showAll($collection, $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json($collection, $code);

    }

    protected function showOne($model, $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json($model, $code);
    }

}
