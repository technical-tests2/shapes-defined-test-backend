<?php

namespace App\Models\Dashboard;


use App\Models\AccessManagement\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Board extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tasks(){
        return $this->hasMany(Task::class,'board_id','id')->orderBy('sort_order');
    }


    public function transformList(Collection $boards): array
    {
        $result = array();
        foreach ($boards as $board) {
            array_push($result, $board->transformItem());
        }
        return $result;
    }

    public function transformItem(): array
    {

        return array(
            'id' => $this->id,
            'name' => $this->name,
            'date' => date('Y-m-d h:i:s a', strtotime($this->created_at)),
            'user' => $this->user->transformItem(),
            'tasks' => $this->getTasks()
        );
    }

    public function getTasks(){
        $taskModal = new Task();
        return $taskModal->transformList($this->tasks);
    }


}
