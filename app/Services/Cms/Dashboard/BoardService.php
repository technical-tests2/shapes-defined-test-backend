<?php

namespace App\Services\Cms\Dashboard;


use App\Models\Dashboard\Board;

class BoardService
{
    public function create($data)
    {
        $data['user_id'] = auth()->guard('cms')->user()->id;
        $board =Board::create($data);
        return $board->transformItem();
    }

    public function delete($id): bool
    {
        $board = Board::where(['id' =>$id,'user_id'=>auth()->guard('cms')->user()->id])->get()->first();
        if($board){
            $board->delete();
            return true;
        }
        return false;

    }

    public function getAll(){
       $user_id = auth()->guard('cms')->user()->id;
       $boards = Board::where('user_id',$user_id)->orderBy('created_at')->get();
       $boardModal = new Board();
       return $boardModal->transformList($boards);
    }
}
