<?php

namespace App\Services\Cms\Dashboard;


use App\Models\Dashboard\Task;

class TaskService
{
    public function create($data)
    {
        $data['user_id'] = auth()->guard('cms')->user()->id;
        $data['sort_order'] = Task::where('board_id', $data['board_id'])->count();
        $task = Task::create($data);
        return $task->transformItem();
    }

    public function updateTaskSortOrder($data): bool
    {
        $task = Task::where(['id' => $data['task_id'], 'user_id' => auth()->guard('cms')->user()->id])->get()->first();
        if ($task) {
            Task::where('board_id', $data['new_board_id'])->where('sort_order', '>=', $data['new_sort_order'])->increment('sort_order');
            $task->sort_order = $data['new_sort_order'];
            $task->board_id = $data['new_board_id'];
            $task->save();
            Task::where('board_id', $data['old_board_id'])->where('sort_order', '>=', $data['old_index'])->decrement('sort_order');
            return true;
        }
        return false;
    }

    public function delete($id): bool
    {
        $task = Task::where(['id' => $id, 'user_id' => auth()->guard('cms')->user()->id])->get()->first();
        if ($task) {
            $task->delete();
            Task::where('board_id', $task->board_id)->where('sort_order', '>=', $task->sort_order)->decrement('sort_order');
            return true;
        }
        return false;

    }

}
