<?php

namespace App\Http\Requests\Cms\AccessManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required'],
            'last_name' => [''],
            'email' => ['required', Rule::unique('users','email')->ignore($this->id, 'id')],
            'is_active' => [''],
        ];
    }
}
