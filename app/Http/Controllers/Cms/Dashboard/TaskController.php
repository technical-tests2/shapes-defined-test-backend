<?php

namespace App\Http\Controllers\Cms\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\Dashboard\TaskRequest;
use App\Http\Requests\Cms\Dashboard\UpdateTaskSortOrderRequest;
use App\Services\Cms\Dashboard\TaskService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;


class TaskController extends Controller
{
    use ApiResponse;

    protected $taskService;

    function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }


    public function create(TaskRequest $request): JsonResponse
    {
        $data = $request->validated();
        $task = $this->taskService->create($data);
        return $this->showOne($task, 201);
    }

    public function updateTaskSortOrder(UpdateTaskSortOrderRequest $request): JsonResponse
    {
        $data = $request->validated();
        if ($this->taskService->updateTaskSortOrder($data)) {
            return $this->successMessage('task move successfully', 200);
        }
        return $this->errorResponse('task not found ', 404);
    }

    public function delete($id): JsonResponse
    {

        if ($this->taskService->delete($id)) {
            return $this->successMessage('task deleted successfully', 200);
        }
        return $this->errorResponse('task not found ', 404);
    }
}
