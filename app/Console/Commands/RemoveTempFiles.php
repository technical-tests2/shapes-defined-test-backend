<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class RemoveTempFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:tempFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'remove files that created from 10 days and above from temp folders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("remove file job has been started");

        collect(Storage::disk('public')->listContents('front-temp', true))
            ->each(function ($file) {
                if ($file['type'] == 'file' && $file['timestamp'] < now()->subDays(10)->getTimestamp()) {
                    Storage::disk('public')->delete($file['path']);
                }
            });

        collect(Storage::disk('local')->listContents('front-temp', true))
            ->each(function ($file) {
                if ($file['type'] == 'file' && $file['timestamp'] < now()->subDays(10)->getTimestamp()) {
                    Storage::disk('local')->delete($file['path']);
                }
            });

        Log::info("remove file job has been finished");
    }
}
