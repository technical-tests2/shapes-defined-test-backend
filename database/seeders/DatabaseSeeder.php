<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                [
                    'first_name' =>'Admin',
                    'last_name' =>'Admin',
                    'email' => 'admin@admin.com',
                    'password' => Hash::make('admin'),
                    'is_active' => 1,
                ],
                [
                    'first_name' =>'Admin 2',
                    'last_name' =>'Admin 2',
                    'email' => 'admin2@admin.com',
                    'password' => Hash::make('admin'),
                    'is_active' => 1,
                ],
            ]
        );
    }
}
