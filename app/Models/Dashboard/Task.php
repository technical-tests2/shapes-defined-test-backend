<?php

namespace App\Models\Dashboard;


use App\Models\AccessManagement\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Task extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','user_id','board_id','sort_order'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }


    public function transformList(Collection $tasks): array
    {
        $result = array();
        foreach ($tasks as $task) {
            array_push($result, $task->transformItem());
        }
        return $result;
    }

    public function transformItem(): array
    {

        return array(
            'id' => $this->id,
            'sort_order' => $this->sort_order,
            'name' => $this->name,
            'date' => date('Y-m-d h:i:s a', strtotime($this->created_at)),
            'user' => $this->user->transformItem(),
        );
    }


}
