<?php

namespace App\Http\Controllers\Cms\AccessManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\AccessManagement\UserRequest;
use App\Models\AccessManagement\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    use ApiResponse;

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (!$token = auth()->guard('cms')->attempt($credentials)) {
            return $this->errorResponse("Unauthorized, Email or password not correct", 401);
        }

        if (auth()->guard('cms')->user()->is_active == 0) {
            return $this->errorResponse('Your Account has been deactivated', 401);
        }

        return response()->json([
            'access_token' => $token,
            'user' => auth()->guard('cms')->user()->transformItem()
        ]);

    }

    public function me(): \Illuminate\Http\JsonResponse
    {
        if (auth()->guard('cms')->user()->is_active == 0) {
            return $this->errorResponse('Your Account has been deactivated', 401);
        } else {
            return $this->showOne((auth()->guard('cms')->user()->transformItem()));
        }
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->guard('cms')->logout();

        return $this->successMessage("Successfully logged out", 200);
    }


    public function changeMyPassword(Request $request): \Illuminate\Http\JsonResponse
    {

        $user = User::findOrFail(auth()->guard('cms')->user()->id);

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
            ], 422);
        }

        if (Hash::check($request['old_password'], $user->password)) {
            $user->password = $request['new_password'];
            $user->save();
            return $this->successMessage("Password updated successfully", 200);
        } else {

            $validator->after(function ($validator) {
                $validator->errors()->add('old_password', "Your old password is wrong");
            });

            if ($validator->fails()) {
                return response()->json([
                    "errors" => $validator->errors(), "message" => "Please complete the missing fields.",
                ], 422);
            }
        }
    }

    public function updateProfile(UserRequest $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validated();
        $user = User::findOrFail(auth()->guard('cms')->user()->id);
        $user->update($data);
        return $this->successMessage("Profile updated successfully", 200);
    }

}
